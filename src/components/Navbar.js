import React from "react";
// import { Link } from "react-router-dom";
import CameraAltOutlinedIcon from "@mui/icons-material/CameraAltOutlined";
// import { grey } from "@mui/material/colors";
import { useHistory } from "react-router-dom";

const Navbar = () => {
  let history = useHistory();
  return (
    <div className="container">
      <div className="l-navbar">
        <li className="nav-item">
          <p>
            <CameraAltOutlinedIcon />
          </p>
        </li>
        <li className="nav-item">
          <p onClick={()=>{history.push("/")}}>Blog App</p>
        </li>

        <li className="nav-item">
          <p onClick={()=>{history.push("/about")}}>About</p>
        </li>

        <li className="nav-item">
          <p onClick={()=>{history.push("/memory")}}>Memory</p>
        </li>
      </div>
      <div className="r-navbar">
        <li className="nav-item">
          <p onClick={()=>{history.push("/login")}}>Login</p>
        </li>
      </div>
    </div>
  );
};

export default Navbar;
