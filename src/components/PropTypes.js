import React from 'react'
import PropTypes from 'prop-types'


const TestPropTypes = ({card}) => {
  return (
    <div>
        {card}
    </div>
  )
}

TestPropTypes.propTypes={
    card : PropTypes.array
}

export default TestPropTypes
